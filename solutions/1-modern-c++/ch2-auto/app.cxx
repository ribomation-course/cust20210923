#include <iostream>
#include <tuple>
#include <map>
using namespace std;

auto fib(int n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

auto compute(int n) {
    return make_tuple(n, fib(n));
}

struct Pair {int arg; unsigned result;};
auto compute2(int n) {
    return Pair{n, fib(n)};
}

auto populate(int n) {
    auto      result = map<int, unsigned>{};
    for (auto k = 1; k <= n; ++k) {
        auto[a, f] = compute(k);
        result[a] = f;
    }
    return result;
}

int main() {
    auto result = fib(10);
    cout << "result: " << result << endl;

    {
        auto[a, f] = compute(10);
        cout << "a=" << a << ", f=" << f << endl;
    }
    {
        auto[a, f] = compute2(10);
        cout << "a=" << a << ", f=" << f << endl;
    }
    auto tbl = populate(10);
    for (auto [arg, fibval] : tbl) cout << "fib(" << arg << ") = " << fibval << endl;
}
