#include <iostream>
#include <string>
#include <vector>
using namespace std;
using namespace std::literals;

int main() {
    vector<string> words = {
            "Hi "s, "Hello\0_\0World"s, "Howdy"s, "Tjabba"s
    };
    for (string    w: words) cout << "[" << w << "]" << " ";
    cout << endl;

    string            sentence = "It was a dark and stormy night. Suddenly, there was a sound of a shot."s;
    if (auto idx      = sentence.find("of"s); idx != string::npos) {
        cout << "found: \"" << sentence.substr(idx) << "\"" << endl;
    } else {
        cout << "'of' not found\n";
    }

    if (unsigned long idx = sentence.find("X"s); idx != string::npos) {
        cout << "found: \"" << sentence.substr(idx) << "\"" << endl;
    } else {
        cout << "'X' not found\n";
    }
}
