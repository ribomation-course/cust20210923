#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,12};
    const auto N         = sizeof(numbers) / sizeof(numbers[0]);

    for_each(numbers, numbers + N, [](int n){cout<<n<<" ";});
    cout << endl;

    auto factor = 42;
    int dest[N];
    transform(numbers, numbers + N, dest, [factor](int n){return factor*n;});

    for_each(dest, dest + N, [](int n){cout<<n<<" ";});
    cout << endl;
}
