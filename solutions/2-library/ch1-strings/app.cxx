#include <iostream>
#include <string>
#include <cstring>

using namespace std;

auto strip(string s) -> string;

int main() {
    auto txt = "  C++ is # c00o00ol la4nguag3e !!"s;
    cout << "[" << txt << "] --> [" << strip(txt) << "]\n";
}

auto strip(string s) -> string {
    auto      result = string{};
    for (auto ch: s) if (isalpha(ch)) {
        //result.push_back(ch);
        result += ch;
    }
    return result;
}
