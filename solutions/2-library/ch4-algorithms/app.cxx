#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
using namespace std;

auto strip(string s) -> string;

int main() {
    auto txt = "  C++ is # c00o00ol la4nguag3e !!"s;
    cout << "[" << txt << "] --> [" << strip(txt) << "]\n";
}

auto strip(string s) -> string {
    auto last = remove_if(s.begin(), s.end(), [](auto ch) {
        return !isalpha(ch);
    });
    s.erase(last, s.end());
    return s;
}
