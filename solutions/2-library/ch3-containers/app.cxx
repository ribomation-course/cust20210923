#include <iostream>
#include <string>
#include <set>

using namespace std;

int main() {
    auto        words = set<string>{};
    for (string word; cin >> word;) words.insert(word);
    for (auto const& w: words) cout << "[" << w << "]" << " ";
    cout << endl;
}
