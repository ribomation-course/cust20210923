#include <iostream>
#include <string>
#include <optional>
using namespace std;

struct Contact {
    string name;
    optional<string> email;
    optional<string> street;
    optional<string> code;
    optional<string> city;

    Contact(string n) : name{n} {}
    auto setEmail(string s)  { email = s;  return *this; }
    auto setStreet(string s) { street = s; return *this; }
    auto setCode(string s)   { code = s;   return *this; }
    auto setCity(string s)   { city = s;   return *this; }

    auto toString() const {
        return "Contact["s
            + name
            + (email  ? ", "s + *email  : ""s)
            + (street ? ", "s + *street : ""s)
            + (code   ? ", "s + *code   : ""s)
            + (city   ? ", "s + *city   : ""s)
            + "]"s;
    }
};

int main() {
    cout << Contact{"Nisse"}.toString() << endl;
    cout << Contact{"Anna"}.setEmail("anna@gmail.com"s).toString() << endl;
    cout << Contact{"Berra"}
            .setStreet("42 Hacker Lane"s)
            .setCode("PWA JS"s)
            .setCity("Perl Lake"s)
            .toString() << endl;
}
