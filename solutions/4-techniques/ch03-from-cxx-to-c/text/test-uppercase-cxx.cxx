#include <iostream>

extern auto toUpperCase(const char* txt) -> const char*;

int main() {
    using std::cout;
    auto txt = "tjabba habba babba";
    auto TXT = toUpperCase(txt);

    cout << "[C++] \"" << txt << "\" --> \"" << TXT << "\"\n";
    delete TXT;

    return 0;
}
