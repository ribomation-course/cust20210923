cmake_minimum_required(VERSION 3.16)
project(ch23_cxx_into_c)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 99)


add_executable(text-cxx
        text/uppercase.cxx
        text/test-uppercase-cxx.cxx
)
add_executable(text-c
        text/uppercase.cxx
        text/test-uppercase-c.c
)


add_executable(object-cxx
        object/person.hxx
        object/person.cxx
        object/test-person-cxx.cxx
)
add_executable(object-c
        object/person.cxx
        object/test-person-c.c
)

