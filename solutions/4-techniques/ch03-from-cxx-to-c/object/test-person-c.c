#include <stdio.h>
#include <stdlib.h>

// [1] define a struct type (we don't need to content)
struct Person;

// [2] find the linker names
// nm `find . -name '*person*.o'` | grep -i person
// ./cmake-build-debug/CMakeFiles/object-cxx.dir/object/person.cxx.o:
// 00000000000001e6 T _Z9newPersonPKcj          - newPerson(char const*, unsigned)
// 0000000000000000 T _ZN6PersonC1EPKcj         - Person::Person(const char*, unsigned)
// 0000000000000138 T _ZN6PersonD1Ev            - Person::~Person()
// 0000000000000192 T _ZNK6Person7getNameEv     - Person::getName
// 00000000000001b0 T _ZNK6Person6getAgeEv      - Person::getAge

// [3] define extern decls
extern struct Person* _Z9newPersonPKcj(const char*, unsigned);
extern struct Person* _ZN6PersonC1EPKcj(void*, const char*, unsigned);
extern void* _ZN6PersonD1Ev(struct Person*);
extern const char* _ZNK6Person7getNameEv();
extern unsigned _ZNK6Person6getAgeEv();

int main() {
    printf("Welcome to C\n");
    {
        struct Person* p = _Z9newPersonPKcj("Anna Conda", 42);
        printf("p1: Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));
        free(_ZN6PersonD1Ev(p));
    }
    {
        size_t sizeOfPerson = 40;
        struct Person* p = malloc(sizeOfPerson);
        _ZN6PersonC1EPKcj(p, "Nisse Hult", 37);
        printf("p2: Person{%s, %d}\n", _ZNK6Person7getNameEv(p), _ZNK6Person6getAgeEv(p));
        free(_ZN6PersonD1Ev(p));
    }
    return 0;
}
