#pragma once

#include <type_traits>
#include <limits>

namespace ribomation::bits {

    template<typename DstType, typename SrcType>
    inline DstType pack(SrcType hiHalf, SrcType loHalf) {
        static_assert(std::is_unsigned_v<SrcType>, "src-type must be unsigned");
        static_assert(std::is_unsigned_v<DstType>, "dst-type must be unsigned");

        constexpr int SRC_DIGITS = std::numeric_limits<SrcType>::digits;
        constexpr int DST_DIGITS = std::numeric_limits<DstType>::digits;
        static_assert((2 * SRC_DIGITS) == DST_DIGITS, "src-type not half of dst-type");

        return static_cast<DstType>( static_cast<DstType>(hiHalf) << SRC_DIGITS | loHalf );
    }

}

