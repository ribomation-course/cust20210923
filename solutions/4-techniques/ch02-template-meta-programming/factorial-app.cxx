#include <iostream>

using namespace std;

constexpr auto factorial(unsigned n) -> unsigned long {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return n * factorial(n - 1);
}

int main() {
    unsigned long tbl[] = {
            factorial(1),
            factorial(2),
            factorial(3),
            factorial(4),
            factorial(5),
            factorial(6),
            factorial(7),
            factorial(8),
            factorial(9),
            factorial(10)
    };

    for (auto f: tbl) cout << f << "\n";
}

//$ objdump --source ./cmake-build-debug/factorial-app
//int main() {
//    11c9:       f3 0f 1e fa             endbr64
//    11cd:       55                      push   %rbp
//    11ce:       48 89 e5                mov    %rsp,%rbp
//    11d1:       48 83 c4 80             add    $0xffffffffffffff80,%rsp
//    11d5:       64 48 8b 04 25 28 00    mov    %fs:0x28,%rax
//    11dc:       00 00
//    11de:       48 89 45 f8             mov    %rax,-0x8(%rbp)
//    11e2:       31 c0                   xor    %eax,%eax
//    unsigned long tbl[] = {
//            11e4:       48 c7 45 a0 01 00 00    movq   $0x1,-0x60(%rbp)
//            11eb:       00
//            11ec:       48 c7 45 a8 02 00 00    movq   $0x2,-0x58(%rbp)
//            11f3:       00
//            11f4:       48 c7 45 b0 06 00 00    movq   $0x6,-0x50(%rbp)
//            11fb:       00
//            11fc:       48 c7 45 b8 18 00 00    movq   $0x18,-0x48(%rbp)
//            1203:       00
//            1204:       48 c7 45 c0 78 00 00    movq   $0x78,-0x40(%rbp)
//            120b:       00
//            120c:       48 c7 45 c8 d0 02 00    movq   $0x2d0,-0x38(%rbp)
//            1213:       00
//            1214:       48 c7 45 d0 b0 13 00    movq   $0x13b0,-0x30(%rbp)
//            121b:       00
//            121c:       48 c7 45 d8 80 9d 00    movq   $0x9d80,-0x28(%rbp)
//            1223:       00
//            1224:       48 c7 45 e0 80 89 05    movq   $0x58980,-0x20(%rbp)
//            122b:       00
//            122c:       48 c7 45 e8 00 5f 37    movq   $0x375f00,-0x18(%rbp)
//            1233:       00
//            factorial(8),
//            factorial(9),
//            factorial(10)
//    };


