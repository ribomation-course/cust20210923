#pragma once
#include <string>

namespace ribomation {
    using namespace std;

    class Thing {
        static int count;
        string     name;
    public:
        Thing() { ++count; }
        Thing(const Thing& rhs) : name{rhs.name} { ++count; }
        ~Thing() { --count; }
        static int getCount() { return count; }
    };
}
