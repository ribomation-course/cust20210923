#include <iostream>
#include <vector>
#include "thing.hxx"

using namespace std;
using namespace ribomation;

int main() {
    auto const N = 1'000'000;
    {
        cout << "#Thing: " << Thing::getCount() << endl;
        auto      vec = vector<Thing>{};
        for (auto k   = 0; k < N; ++k) vec.push_back(Thing{});
        cout << "#Thing: " << Thing::getCount() << endl;
    }
    cout << "#Thing: " << Thing::getCount() << endl;
}
