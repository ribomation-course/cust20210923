#pragma once
#include <string>
#include "dog.hxx"

namespace ribomation {
    using namespace std;
    using namespace ribomation;

    class Person {
        string name;
        Dog* dog = nullptr;

    public:
        Person(string name) : name{move(name)} {}
        void setDog(Dog* d) { Person::dog = d; }
        string toString() const {
            return "Person{"s + name + (dog ? ", "s + dog->toString() : ""s) + "}"s;
        }
    };
}
