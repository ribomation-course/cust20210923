#pragma once
#include <string>

namespace ribomation {
    using namespace std;

    class Dog {
        const string name;
    public:
        Dog(string name) : name{move(name)} {}
        string toString() const {
            return "Dog{"s + name + "}"s;
        }
    };
}
