#include <iostream>
#include "dog.hxx"
#include "person.hxx"

using namespace std;
using namespace ribomation;

int main() {
    auto nisse = Person{"Nisse Hult"s};
    cout << "(A) " << nisse.toString() << endl;

    auto fido = Dog{"Fido"s};
    nisse.setDog(&fido);
    cout << "(B) " << nisse.toString() << endl;
}
