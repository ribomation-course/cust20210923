#include <iostream>
#include <filesystem>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include "count.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::io;

namespace fs = std::filesystem;
using FileStats = unordered_map<string, Count>;

bool isTextFile(const fs::path& p) {
    static const unordered_set<string> TEXT_EXTS = {
            ".txt", ".cxx", ".cpp", ".hxx", ".log",
            ".h", ".c", ".cmake", ".make"
    };
    return fs::is_regular_file(p) && TEXT_EXTS.count(p.extension().string()) > 0;
}

int main(int argc, char** argv) {
    auto dir = fs::path{argc == 1 ? ".." : argv[1]};
    if (!fs::is_directory(dir)) { dir = fs::current_path(); }

    cout << "Dir: " << fs::canonical(dir) << endl;

    Count total{"TOTAL"s};
    for (const auto& e : fs::recursive_directory_iterator{dir}) {
        const auto& p = e.path();
        if (isTextFile(p)) {
            Count cnt{p};
            cout << cnt << endl;
            total += cnt;
        }
    }
    cout << total << endl;
    return 0;
}
