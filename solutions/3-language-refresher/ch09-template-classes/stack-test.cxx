#include <iostream>
#include <string>
#include "stack.hxx"

using namespace std;
using namespace std::literals;

struct Person {
    string name;
    Person(string n) : name{n} {}
    Person() = default;
    Person& operator =(const Person&) = default;
};

auto operator <<(ostream& os, const Person& p) -> ostream& {
    return os << "Person(" << p.name << ") @ " << &p;
}

template<typename Collection>
void drain(Collection& coll) {
    while (!coll.empty())
        cout << coll.pop() << endl;
}

int main() {
    {
        auto s = Stack<int, 5>{};
        cout << "sizeof(int) = " << sizeof(int) << endl;
        cout << "sizeof(Stack<int, 5>) = " << sizeof(s) << endl;

        for (auto k = 1; !s.full(); ++k)
            s.push(k * 10);
        drain(s);
    }
    cout << "----------\n";
    {
        auto s = Stack<string, 5>{};
        cout << "sizeof(string) = " << sizeof(string) << endl;
        cout << "sizeof(Stack<string, 5>) = " << sizeof(s) << endl;

        for (auto k = 1; !s.full(); ++k)
            s.push("nisse-"s + to_string(k));
        drain(s);
    }
    cout << "----------\n";
    {
        auto s = Stack<Person, 5>{};
        cout << "sizeof(Person) = " << sizeof(Person) << endl;
        cout << "sizeof(Stack<Person, 5>) = " << sizeof(s) << endl;

        for (auto k = 1; !s.full(); ++k)
            s.push(Person{"anna-"s + to_string(k)});
        cout << "---\n";
        drain(s);
        cout << "---\n";
    }

    return 0;
}

