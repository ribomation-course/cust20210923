#pragma once

#include <stdexcept>

template<typename TYPE, unsigned CAPACITY>
class Stack {
    TYPE stk[CAPACITY];
    int  top = 0;

public:
    unsigned capacity() const { return CAPACITY; }
    int      size()     const { return top; }
    bool     empty()    const { return top == 0; }
    bool     full()     const { return top == CAPACITY; }

    void push(TYPE x) {
        if (full()) throw std::overflow_error{"stack full"};
        stk[top++] = x;
    }

    TYPE pop() {
        if (empty()) throw std::underflow_error{"stack empty"};
        return stk[--top];
    }
};
