#include <iostream>
#include <string>
#include <cassert>
#include "equals.hxx"
#include "person.hxx"
using namespace std;

int main() {
    assert(equals(42, 40 + 2, 2 * 21));

    assert(equals(42ULL, 40ULL + 2ULL, 2ULL * 21ULL));

    assert(equals("wifi"s, "wi"s + "fi"s, "**wifi**"s.substr(2, 4)));

    assert(equals(Person{"nisse"}, Person{"nisse"}, Person{"nisse"}));

    assert(equals<char>('A', 'B' - 1, 'C' - 2));

    cout << "All tests passed\n";
}
