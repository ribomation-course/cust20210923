#pragma once

#include <string>
#include <iosfwd>
#include <utility>

namespace ribomation {
    using namespace std;

    class Person {
        string      name;
        mutable int age;
    public:
        Person(string n, int a) : name{std::move(n)}, age{a} {}

        int incrAge() const {
            return ++age;
        }

        friend auto operator<<(ostream& os, const Person& p) -> ostream& {
            return os << "Person{" << p.name << ", " << p.age << "}";
        }
    };

}

