#pragma once

#include <iostream>
#include <sstream>
#include <cstring>

namespace ribomation {
    using namespace std;

    class Person {
        char* name = nullptr;
        unsigned age = 0;

        static char* copystr(const char* str) {
            if (str == nullptr) return nullptr;
            return strcpy(new char[strlen(str) + 1], str);
        }

    public:
        ~Person() {
            cout << "~Person @ " << this << endl;
            delete[] name;
        }

        Person() {
            cout << "Person{} @ " << this << endl;
            name = copystr("");
        }

        Person(const char* n, unsigned a) : name{copystr(n)}, age{a} {
            cout << "Person{const char* " << n << ", " << a << "} @ " << this << endl;
        }

        Person(const Person& that) = delete;
        Person& operator=(const Person& that) = delete;

        Person(Person&& that) noexcept : name{that.name}, age{that.age} {
            that.name = nullptr;
            that.age = 0;
            cout << "Person{Person&& " << &that << "} @ " << this << endl;
        }

        Person& operator=(Person&& rhs) noexcept {
            if (this != &rhs) {
                delete [] name;
                name = rhs.name;
                rhs.name = nullptr;
                age = rhs.age;
                rhs.age = 0;
            }
            cout << "operator={Person&& " << &rhs << "} @ " << this << endl;
            return *this;
        }

        string toString() const {
            ostringstream buf{};
            buf << "Person(" << (name ? name : "??") << ", " << age << ") @ " << this;
            return buf.str();
        }

        unsigned incrAge() {
            return ++age;
        }
    };

}
