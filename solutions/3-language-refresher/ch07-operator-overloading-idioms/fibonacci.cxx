#include <iostream>
#include <iomanip>
using namespace std;
using ValueType = unsigned long long;

class Fibonacci {
    const unsigned n;
public:
    explicit Fibonacci(unsigned n) : n{n} {}

    struct Iterator {
        ValueType f2 = 0;
        ValueType f1 = 1;
        unsigned  n;

        Iterator(unsigned n) : n{n} {}

        bool operator !=(const Iterator& rhs) const {
            return n != rhs.n;
        }

        Iterator& operator ++() {
            --n;
            return *this;
        }

        ValueType operator *() {
            ValueType f = f1 + f2;
            f2 = f1;
            f1 = f;
            return f2;
        }
    };

    Iterator begin() const { return {n}; }
    Iterator end()   const { return {0}; }
};

int main() {
    for (auto f : Fibonacci{45}) cout << setw(12) << f << endl;
    //for (Fibonacci::Iterator it = fib.begin(); it != fib.end(); ++it) {auto n = *it; ...}
}
