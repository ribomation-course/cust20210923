#include <iostream>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    cout << "[func] q: " << q.toString() << endl;
    q.incrAge();
    cout << "[func] q: " << q.toString() << endl;
    return q;
}

Person g{"Anna Conda", 42};

int main() {
    cout << "[main] enter\n";
    cout << "[main] g: " << g.toString() << endl;

    cout << "[main] -- block before ----\n";
    {
        auto p = Person{"Cris P. Bacon", 27};
        cout << "[main] p: " << p.toString() << endl;

        cout << "[main] -- func ----\n";
        auto q = func(p);
        cout << "[main] q: " << q.toString() << endl;
    }
    cout << "[main] -- block after ----\n";

    cout << "[main] -- vector block before --\n";
    {
        auto v = vector<Person>{{"Anna",  27},
                                {"Berit", 37},
                                {"Carin", 47}};

        cout << "[main] -- copy print ----\n";
        for (auto p : v) cout << "[copy] p: " << p.toString() << endl;

        cout << "[main] -- ref print ----\n";
        for (auto const& p : v) cout << "[ref]  p: " << p.toString() << endl;
        cout << "[main] -- just before end of block ----\n";
    }
    cout << "[main] -- vector block after --\n";

    cout << "[main] exit\n";
    return 0;
}
