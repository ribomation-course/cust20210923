cmake_minimum_required(VERSION 3.14)
project(14_constructors)

set(CMAKE_CXX_STANDARD 17)

add_executable(persons
        person.hxx
        app.cxx)

