#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <filesystem>
#include <stdexcept>
#include <string>
#include <cctype>
#include <vector>
#include <unordered_map>
#include <algorithm>
#include <random>
#include <chrono>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;
namespace ch = std::chrono;

using WordFreqs = unordered_map<string, unsigned>;
using WordFreq  = pair<string, unsigned>;
using Pairs     = vector<WordFreq>;
using Strings   = vector<string>;

auto load(const string& filename, unsigned minSize) -> WordFreqs;
auto strip(string line) -> Strings;
auto mostFrequent(const WordFreqs& freqs, unsigned maxWords) -> Pairs;
auto asTags(Pairs pairs) -> Strings;
auto randColor() -> string;
auto store(const Strings& tags, const string& infile, const string& outdir) -> string;

static auto r = random_device{};

int main(int argc, char** argv) {
    auto filename = "../files/musketeers.txt"s;
    auto maxWords = 100U;
    auto minSize  = 5U;
    auto outdir   = "./"s;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-f"s) {
            filename = argv[++k];
        } else if (arg == "-d"s) {
            outdir = argv[++k];
        } else if (arg == "-w"s) {
            maxWords = stoi(argv[++k]);
        } else if (arg == "-s"s) {
            minSize = stoi(argv[++k]);
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << argv[0] << " [-f <file>] [-w <max-words>] [-s <min-size>]" << endl;
            return 1;
        }
    }

    cout << "Loading " << filename << ", " << fs::file_size(filename) / (1024.0 * 1024) << " MB" << endl;

    auto startTime = ch::high_resolution_clock::now();
    auto wordFreqs = load(filename, minSize);
    auto pairs     = mostFrequent(wordFreqs, maxWords);
    auto tags      = asTags(pairs);
    auto htmlFile  = store(tags, filename, outdir);
    auto endTime   = ch::high_resolution_clock::now();

    cout << "Loaded " << wordFreqs.size() << " words" << endl;
    cout << "Written " << htmlFile << endl;
    cout << "Elapsed " << ch::duration_cast<ch::milliseconds>(endTime - startTime).count() << " ms" << endl;

    return 0;
}

auto load(const string& filename, unsigned minSize) -> WordFreqs {
    auto file = ifstream{filename};
    if (!file) {
        throw invalid_argument{"cannot open "s + filename};
    }

    auto        freqs = WordFreqs{};
    for (string line; getline(file, line);) {
        auto words = strip(line);
        for (const auto& word : words) {
            if (word.size() >= minSize) ++freqs[word];
        }
    }

    return freqs;
}

auto mostFrequent(const WordFreqs& freqs, unsigned maxWords) -> Pairs {
    auto sortable = Pairs{begin(freqs), end(freqs)};
    sort(begin(sortable), end(sortable), [](const WordFreq& lhs, const WordFreq& rhs) {
        return lhs.second > rhs.second;
    });

    auto pairs = Pairs{};
    pairs.reserve(maxWords);
    copy_n(begin(sortable), maxWords, back_inserter(pairs));

    return pairs;
}

auto asTags(Pairs pairs) -> Strings {
    auto tags = Strings{};
    tags.reserve(pairs.size());

    auto maxFreq     = pairs.front().second;
    auto minFreq     = pairs.back().second;
    auto maxFontSize = 150.0;
    auto minFontSize = 15.0;
    auto scale       = (maxFontSize - minFontSize) / (maxFreq - minFreq);

    transform(begin(pairs), end(pairs), back_inserter(tags), [=](const WordFreq& wf) {
        auto word     = wf.first;
        auto freq     = wf.second;
        auto normFreq = static_cast<int>(scale * freq );
        auto buf      = ostringstream{};
        buf << "<span style='font-size:" << normFreq << "px;"
            << " color:" << randColor() << ";'>" 
            << word 
            << "</span>";
        return buf.str();
    });
    shuffle(begin(tags), end(tags), r);

    return tags;
}

auto store(const Strings& tags, const string& infile, const string& outdir) -> string {
    auto htmlFile = fs::path(outdir + fs::path(infile).stem().string() + ".html"s);
    auto file     = ofstream{htmlFile};
    if (!file) {
        throw runtime_error{"cannot open "s + htmlFile.string() + " for writing"s};
    }

    file << R"(<html>
    <head>
        <title>Word Cloud</title>
    </head>
    <body>
        <h1>Word Cloud</h1>
    )";
    for (const auto& tag : tags) file << tag << "\n";
    file << "</body></html>";

    return htmlFile;
}

auto strip(string line) -> Strings {
    transform(begin(line), end(line), begin(line), [](auto ch) {
        return isalpha(ch) ? tolower(ch) : ' ';
    });
    auto        buf   = istringstream{line};
    auto        words = Strings{};
    for (string word; buf >> word;) words.push_back(word);
    return words;
}

auto randColor() -> string {
    auto val = uniform_int_distribution{0, 255};
    auto HEX = [&]() {
        auto buf = ostringstream{};
        buf << setw(2) << setfill('0') << hex << uppercase << val(r);
        return buf.str();
    };
    return "#"s + HEX() + HEX() + HEX();
}
