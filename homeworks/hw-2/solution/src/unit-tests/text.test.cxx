#include "catch.hpp"
#include "text.hxx"
#include <sstream>
using namespace ribomation;
using namespace std;
using namespace std::literals;

TEST_CASE("default constructor should return empty string", "[text]") {
    auto target = Text<4>{};
    REQUIRE(target.value() == ""s);
    REQUIRE(target.value().size() == 0);
}

TEST_CASE("insert payload with size eq capacity", "[text]") {
    auto target = Text<4>{};
    target.value("abcd"s);
    REQUIRE(target.value() == "abcd"s);
}

TEST_CASE("insert payload with size lt capacity", "[text]") {
    auto target = Text<4>{};
    target.value("abc"s);
    REQUIRE(target.value() == "abc"s);
}

TEST_CASE("insert payload with size gt capacity, should truncate", "[text]") {
    auto target = Text<4>{};
    target.value("abcdef"s);
    REQUIRE(target.value() == "abcd"s);
}

TEST_CASE("using its type-conversion operator", "[text]") {
    auto target = Text<4>{};
    target.value("abc"s);
    string s = target;  //target.operator string()
    REQUIRE(s == "abc"s);
}

TEST_CASE("using its assignment-conversion operator", "[text]") {
    auto target = Text<4>{};
    target = "abc"s;
    REQUIRE(target.value() == "abc"s);
}

TEST_CASE("printing to a stream", "[text]") {
    auto target = Text<4>{};
    target.value("oki"s);
    auto buf = ostringstream{};
    buf << target; //operator<<(ostream&, const Text<N>&)
    REQUIRE(buf.str() == "oki"s);
}


