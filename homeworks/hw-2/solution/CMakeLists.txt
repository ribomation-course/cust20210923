cmake_minimum_required(VERSION 3.16)
project(homework_2)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED  ON)
set(CMAKE_CXX_EXTENSIONS   OFF)

set(LIBSRC ${CMAKE_CURRENT_LIST_DIR}/src/lib)

add_library(block-file INTERFACE)
target_sources(block-file INTERFACE
        ${LIBSRC}/text.hxx
        ${LIBSRC}/number.hxx
        ${LIBSRC}/block-file.hxx
        )
target_include_directories(block-file INTERFACE  ${LIBSRC})


add_executable(unit-tests
        src/unit-tests/catch.hpp
        src/unit-tests/test-driver.cxx

        src/unit-tests/text.test.cxx
        src/unit-tests/number.test.cxx
        src/unit-tests/block-file.test.cxx)
target_link_libraries(unit-tests block-file)


add_executable(app-generate)
target_sources(app-generate PUBLIC
        src/app/account.hxx
        src/app/generate.cxx
        )
target_link_libraries(app-generate block-file)

add_executable(app-print)
target_sources(app-print PUBLIC
        src/app/account.hxx
        src/app/print.cxx
        )
target_link_libraries(app-print block-file)

add_executable(app-update)
target_sources(app-update PUBLIC
        src/app/account.hxx
        src/app/update.cxx
        )
target_link_libraries(app-update block-file)
